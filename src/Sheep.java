import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.*;

/**
 * Sheep.
 *
 * @author takkie
 */
public class Sheep extends MIDlet {

  public SheepCanvas canvas;

  public static final int FRAME = 1000;

  public static final int TIME = 60000;

  public Sheep() {

      Canvas dmy = new Canvas() {
	      public void paint(Graphics g) {}
	  };

    canvas = new SheepCanvas(255, 255, dmy.getWidth(), dmy.getHeight());
    canvas.setMIDlet(this);
    canvas.start();

  }

  public void startApp() {

    Display.getDisplay(this).setCurrent(canvas);

  }

  public void pauseApp() {

  }

  public void destroyApp(boolean conditional) {
    notifyDestroyed();
  }

}


